const xmlhttp = new XMLHttpRequest();
xmlhttp.onload = function() {
    const data = JSON.parse(this.responseText);
    let content = ''
    for (let i in data) {
        content += '<div class="product"><img src="' + data[i].imageUrl + '" class="productImage">'
        content += '<h4 class="productName">' + data[i].name + '</h4>'
        content += '<div><select class="productOptions"><option value="0.33l">0.33l</option><option value="0.5l">0.5l</option><option value="1.5l">1.5l</option></select>'
        content += '<h4 class="price">' + data[i].prize + '</h4>'
        content += '<img src="images/shoppingcart.png" class="cartImage" onclick="cartCount()"></div>'
        content += '</div></div>'
    }
    document.getElementById('products').innerHTML = content
};
xmlhttp.open("GET", "data/softdrinks.json");
xmlhttp.send();