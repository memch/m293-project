const xmlhttp = new XMLHttpRequest();
xmlhttp.onload = function() {
    const data = JSON.parse(this.responseText);
    let content = ''
    for (let i in data) {
        content += '<div class="product"><img src="' + data[i].imageUrl + '" class="productImage">'
        content += '<div><h4 class="productName">' + data[i].name + '</h4>'
        content += '<h4 class="price">' + data[i].prize + '</h4>'
        content += '<img src="images/shoppingcart.png" class="cartImage" onclick="cartCount()"></div><p>'

        for (let j = 0; j < data[i].ingredients.length - 1; j++) {
            content += data[i].ingredients[j] + ", "
        }
        content += data[i].ingredients[data[i].ingredients.length - 1]
        content += '</p></div>'
    }
    document.getElementById('products').innerHTML = content
};
xmlhttp.open("GET", "data/pizzas.json");
xmlhttp.send();