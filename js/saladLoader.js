const xmlhttp = new XMLHttpRequest();
xmlhttp.onload = function() {
    const data = JSON.parse(this.responseText);
    let content = ''
    for (let i in data) {
        content += '<div class="product"><img src="' + data[i].imageUrl + '" class="productImage">'
        content += '<h4 class="productName">' + data[i].name + '</h4>'

        for (let j = 0; j < data[i].ingredients.length - 1; j++) {
            content += data[i].ingredients[j] + ", "
        }
        content += data[i].ingredients[data[i].ingredients.length - 1]

        content += '<div><select class="productOptions"><option value="italianDressing">Italian dressing</option><option value="frenchDressing">French dressing</option><option value="noDressing">No dressing</option></select>'
        content += '<h4 class="price">' + data[i].prize + '</h4>'
        content += '<img src="images/shoppingcart.png" class="cartImage" onclick="cartCount()"></div>'

        content += '</div></div>'
    }
    document.getElementById('products').innerHTML = content
};
xmlhttp.open("GET", "data/salads.json");
xmlhttp.send();